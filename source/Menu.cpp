#include <Menu.hpp>

#include <Small/Core/SceneManager.hpp>

Menu::Menu(int id) :
    sgl::Scene(id), m_layout(0, nullptr, sf::IntRect(0, 0, 600, 600))
{
    m_font.loadFromFile("assets/yoster.ttf");

    int labelId = m_layout.attach<sgl::Widgets::Label>(sf::IntRect(260, 20, 0, 0));
    {
        sgl::Widgets::Label* label = reinterpret_cast<sgl::Widgets::Label*>(m_layout[labelId]);
        label->text().setString("Menu");
        label->text().setFont(m_font);
        label->text().setCharacterSize(24);
        label->text().setFillColor(sf::Color::White);
    }

    int label2Id = m_layout.attach<sgl::Widgets::Label>(sf::IntRect(80, 200, 0, 0));
    {
        sgl::Widgets::Label* label = reinterpret_cast<sgl::Widgets::Label*>(m_layout[label2Id]);
        label->text().setString(
            "Utilisez Q et D pour deplacer Abu,\n" \
            "espace pour le faire sauter\n" \
            "\n" \
            "Appuyez sur n'importe quelle touche\n" \
            "pour lancer le jeu"
        );
        label->text().setFont(m_font);
        label->text().setCharacterSize(24);
        label->text().setFillColor(sf::Color::White);
    }
}

void Menu::onEvent(const sf::Event& event)
{
    m_layout.onEvent(event);

    if (event.type == sf::Event::KeyReleased)
        m_sceneManager->setCurrent(1);  // Game
}

void Menu::onUpdate(const sf::Time dt)
{
    m_layout.onUpdate(dt);
}

void Menu::onRender(sf::RenderTarget& screen, const sf::Transform& transform)
{
    screen.draw(m_layout, transform);
}