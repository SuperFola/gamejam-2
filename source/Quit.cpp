#include <Quit.hpp>

Quit::Quit(int id) :
    sgl::Scene(id), m_layout(0, nullptr, sf::IntRect(0, 0, 600, 600))
{
    m_font.loadFromFile("assets/yoster.ttf");

    int labelId = m_layout.attach<sgl::Widgets::Label>(sf::IntRect(230, 270, 0, 0));
    {
        sgl::Widgets::Label* label = reinterpret_cast<sgl::Widgets::Label*>(m_layout[labelId]);
        label->text().setString("Game Over");
        label->text().setFont(m_font);
        label->text().setCharacterSize(24);
        label->text().setFillColor(sf::Color::Red);
    }
}

void Quit::onEvent(const sf::Event& event)
{
    m_layout.onEvent(event);
}

void Quit::onUpdate(const sf::Time dt)
{
    m_layout.onUpdate(dt);
}

void Quit::onRender(sf::RenderTarget& screen, const sf::Transform& transform)
{
    screen.draw(m_layout, transform);
}